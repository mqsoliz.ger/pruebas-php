<?php

    class Person{

        public $name = "Pedro", $age = 20;

        public function speak()
        {
            echo "$this->name de $this->age años de edad esta HABLANDO<br />";
        }

        public function get()
        {
            
        }
    }

    class Adult extends Person{
        
    }

    

    $persona = new Person();

    $adulto = new Adult();

    $adulto->speak();

    $persona->name = "Jose";
    $persona->age = 21;

    $persona->speak();

?>